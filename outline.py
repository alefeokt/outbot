import requests
import configparser
import sqlite3

config = configparser.ConfigParser()
config.read("settings.ini")
url = config["outline"]["url"]


def outkey (user):
    conn = sqlite3.connect('users.db')
    cur = conn.cursor()

    cur.execute("SELECT keyid FROM users WHERE userid='"+str(user)+"';")
    res = cur.fetchone()
    if res:
        keys = requests.get(url+'/access-keys', verify=False).json()
        i = 0
        while i < len(keys['accessKeys']):
            if keys['accessKeys'][i]['id'] == str(res[0]):
                break
            else:
                i += 1
        return keys['accessKeys'][i]['accessUrl']


    else:
        return newkey (user)




def newkey (user):

    conn = sqlite3.connect('users.db')
    cur = conn.cursor()

    key = requests.post(url+'/access-keys', verify=False).json()
    param = dict(name = user)
    requests.put(url+'/access-keys/'+str(key['id'])+'/name', json=param, verify=False).status_code

    param = dict(limit = dict(bytes = int(config["outline"]["limit"])))
    requests.put(url+'/access-keys/'+str(key['id'])+'/data-limit', json=param, verify=False).status_code

    val = (user,key['id'])
    print (val)
    cur.execute("INSERT INTO users (userid, keyid) VALUES (?, ?);",val)
    conn.commit()

    return key['accessUrl']

