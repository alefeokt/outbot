import configparser
import telebot
import hashlib
from outline import *


config = configparser.ConfigParser()
config.read("settings.ini")
token = config["telegram"]["token"]
bot = telebot.TeleBot(token=token)


def convert_bytes(bytes_number):
    tags = ["Byte", "Kilobyte", "Megabyte", "Gigabyte", "Terabyte"]

    i = 0
    double_bytes = bytes_number

    while (i < len(tags) and bytes_number >= 1024):
        double_bytes = bytes_number / 1024.0
        i = i + 1
        bytes_number = bytes_number / 1024

    return str(round(double_bytes, 2)) + " " + tags[i]

@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
	bot.reply_to(message, config["msg"]["help"])

@bot.message_handler(commands=['key'])
def getkey(message):
    hash = hashlib.sha256()
    hash.update(bytes(config["telegram"]["salt"]+str(message.from_user.id),'utf-8'))

    r = outkey(hash.hexdigest())
    text = config["msg"]["instr"]
    bot.reply_to(message,text.format(accessUrl=r))

@bot.message_handler(commands=['limit'])
def getlimit(message):
    hash = hashlib.sha256()
    hash.update(bytes(config["telegram"]["salt"]+str(message.from_user.id),'utf-8'))



bot.infinity_polling()